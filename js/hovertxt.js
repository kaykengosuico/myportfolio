//hover text
document.addEventListener('DOMContentLoaded', function() {
    var videoPlayer = document.getElementById('videoPlayer');
    var noSignalEffect = document.querySelector('.tv-overlay');

    var textTrigger1 = document.getElementById('textTrigger1');
    var textTrigger2 = document.getElementById('textTrigger2');
    var textTrigger3 = document.getElementById('textTrigger3');
    var textTrigger4 = document.getElementById('textTrigger4');
    var textTrigger5 = document.getElementById('textTrigger5');

    
    noSignalEffect.style.display = 'block';
    videoPlayer.pause();

    textTrigger1.addEventListener('mouseenter', function() {
        noSignalEffect.style.display = 'none';
        videoPlayer.src = 'videos/wildspace.mp4'; 
        videoPlayer.load();
        videoPlayer.play();
    });

    textTrigger2.addEventListener('mouseenter', function() {
        noSignalEffect.style.display = 'none';
        videoPlayer.src = 'videos/bang.mp4'; 
        videoPlayer.load();
        videoPlayer.play();
    });

    textTrigger3.addEventListener('mouseenter', function() {
        noSignalEffect.style.display = 'none';
        videoPlayer.src = 'videos/shopnswipe.mp4'; 
        videoPlayer.load();
        videoPlayer.play();
    });
    textTrigger4.addEventListener('mouseenter', function() {
        noSignalEffect.style.display = 'none';
        videoPlayer.src = 'videos/lifeline.mp4'; 
        videoPlayer.load();
        videoPlayer.play();
    });
    textTrigger5.addEventListener('mouseenter', function() {
        noSignalEffect.style.display = 'none';
        videoPlayer.src = 'videos/lunhaw.mp4'; 
        videoPlayer.load();
        videoPlayer.play();
    });

    var boxContainer = document.querySelector('.box-container');
    boxContainer.addEventListener('mouseleave', function() {
        var hovered = document.querySelector('.text-trigger:hover');
        if (!hovered) {
            noSignalEffect.style.display = 'block';
        }
    });
});
document.addEventListener('DOMContentLoaded', function() {
    var textTrigger1 = document.getElementById('textTrigger1');
    var textTrigger2 = document.getElementById('textTrigger2');
    var textTrigger3 = document.getElementById('textTrigger3');
    var textTrigger4 = document.getElementById('textTrigger4');
    var textTrigger5 = document.getElementById('textTrigger5');

    function toggleFontStyle(element) {
        element.addEventListener('mouseenter', function() {
            var textCon = element.querySelector('.text-con');
            if (textCon) {
                textCon.classList.add('hover-change');
            }
        });

        element.addEventListener('mouseleave', function() {
            var textCon = element.querySelector('.text-con');
            if (textCon) {
                textCon.classList.remove('hover-change');
            }
        });
    }

    toggleFontStyle(textTrigger1);
    toggleFontStyle(textTrigger2);
    toggleFontStyle(textTrigger3);
    toggleFontStyle(textTrigger4);
    toggleFontStyle(textTrigger5);
});

