// Link scroll
document.addEventListener('DOMContentLoaded', function() {
    var projectsLink = document.getElementById('projects-link');

    projectsLink.addEventListener('click', function(event) {
        event.preventDefault(); 

        var sectionId = this.getAttribute('href'); 
        var targetSection = document.querySelector(sectionId); 

        if (targetSection) {
            var targetY = targetSection.getBoundingClientRect().top + window.pageYOffset;
            var startingY = window.pageYOffset;
            var distance = targetY - startingY;
            var duration = 800; 

            var startTime = null;
            function scrollToSection(currentTime) {
                if (startTime === null) {
                    startTime = currentTime;
                }
                var timeElapsed = currentTime - startTime;
                var scrollProgress = Math.min(timeElapsed / duration, 1);
                var easing = function(t) { return t<0.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }; 

                window.scrollTo(0, startingY + distance * easing(scrollProgress));

                if (timeElapsed < duration) {
                    requestAnimationFrame(scrollToSection);
                }
            }

            requestAnimationFrame(scrollToSection);
        }
    });
});