//popup
document.addEventListener('DOMContentLoaded', function() {
    var textTrigger1 = document.getElementById('textTrigger1');
    var textTrigger2 = document.getElementById('textTrigger2');
    var textTrigger3 = document.getElementById('textTrigger3');
    var textTrigger4 = document.getElementById('textTrigger4');
    var textTrigger5 = document.getElementById('textTrigger5');
    var popupOverlay = document.getElementById('popupOverlay');
    var popupOverlay2 = document.getElementById('popupOverlay2'); 
    var popupOverlay3 = document.getElementById('popupOverlay3');
    var popupOverlay4 = document.getElementById('popupOverlay4'); 
    var popupOverlay5 = document.getElementById('popupOverlay5'); 
    var closeBtn = document.getElementById('closeBtn');
    var closeBtn2 = document.getElementById('closeBtn2'); 
    var closeBtn3 = document.getElementById('closeBtn3'); 
    var closeBtn4 = document.getElementById('closeBtn4'); 
    var closeBtn5 = document.getElementById('closeBtn5'); 

    textTrigger1.addEventListener('click', function() {
        popupOverlay.style.display = 'block';
    });

    textTrigger2.addEventListener('click', function() {
        popupOverlay2.style.display = 'block'; // Display the new BANG popup
    });
    textTrigger3.addEventListener('click', function() {
        popupOverlay3.style.display = 'block';
    });

    textTrigger4.addEventListener('click', function() {
        popupOverlay4.style.display = 'block'; // Display the new BANG popup
    });
    textTrigger5.addEventListener('click', function() {
        popupOverlay5.style.display = 'block'; // Display the new BANG popup
    });

    closeBtn.addEventListener('click', function() {
        popupOverlay.style.display = 'none';
    });

    closeBtn2.addEventListener('click', function() {
        popupOverlay2.style.display = 'none'; // Close the BANG popup
    });
    closeBtn3.addEventListener('click', function() {
        popupOverlay3.style.display = 'none';
    });

    closeBtn4.addEventListener('click', function() {
        popupOverlay4.style.display = 'none'; // Close the BANG popup
    });
    closeBtn5.addEventListener('click', function() {
        popupOverlay5.style.display = 'none'; // Close the BANG popup
    });
});

